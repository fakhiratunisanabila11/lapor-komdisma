<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Komdisma extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'nip'          => [
				'type'           => 'VARCHAR',
				'constraint'     => 20,
			],
			'id_akun'       => [
				'type'           => 'INT',
				'constraint'	 => 11,
				'auto_increment' => TRUE,
				'unsigned' 		 => true,
			],
			'nama'       => [
				'type'           => 'TEXT',
				'null'			 => TRUE,
			],
		]);
		$this->forge->addKey('nip', TRUE);
		$this->forge->addForeignKey('id_akun','users', 'id', 'CASCADE', 'CASCADE');
		$this->forge->createTable('komdisma');
	}

	public function down()
	{
		$this->forge->dropTable('komdisma');
	}
}
