<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Mahasiswa extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'nim'          => [
				'type'           => 'VARCHAR',
				'constraint'     => 20,
			],
			'id_akun'       => [
				'type'           => 'INT',
				'constraint'	 => 11,
				'auto_increment' => TRUE,
				'unsigned' 		 => true,
			],
			'nama'       => [
				'type'           => 'TEXT',
				'null'			 => TRUE,
			],
			'prodi' => [
				'type'           => 'VARCHAR',
				'constraint'	=> 5,
				'null'			 => TRUE,
			],
			'tgl_surat_kelakuan' => [
				'type'           => 'DATE',
				'null'			 => TRUE,
			],
			'tgl_yudisium' => [
				'type'           => 'DATE',
				'null'			 => TRUE,
			],
		]);
		$this->forge->addKey('nim', TRUE);
		$this->forge->addForeignKey('id_akun','users', 'id', 'CASCADE', 'CASCADE');
		$this->forge->createTable('mahasiswa');
	}

	public function down()
	{
		$this->forge->dropTable('mahasiswa');
	}
}
