<?php

namespace App\Controllers;

class Mahasiswa extends BaseController
{
	protected $db;

	public function __construct()
	{
		$this->db= \Config\Database::connect();
	}

	public function index()
	{	
		$data['title'] = 'Sistem Lapor Komdisma';

		return view('mahasiswa/dashboard',$data);
	}

	public function profile()
	{	
		$data['title'] = 'Sistem Lapor Komdisma';

		$id= user_id();
		$builder = $this->db->table('users');

			$builder->select('username, email, foto, nama, nim, prodi');
			$builder->join('mahasiswa', 'mahasiswa.id_akun= users.id');
			$builder->where('users.id', $id);
			$query = $builder->get();

			$data['user'] = $query->getRow();


		return view('mahasiswa/profile', $data);
	}
}
