<?php

namespace App\Controllers;
use \Myth\Auth\Models\UserModel;
class Admin extends BaseController
{
	protected $db;

	public function __construct()
	{
		$this->db= \Config\Database::connect();
		$this->userModel = new UserModel();
		$this->config = config('auth');
		$this->hashOptions = [
			'memory_cost' => $this->config->hashMemoryCost,
			'time_cost'   => $this->config->hashTimeCost,
			'threads'     => $this->config->hashThreads
			];
		
	}

	// fungsi untuk menampilkan dashboard
	public function index()
	{	
		$data['title'] = 'Sistem Lapor Komdisma';

		return view('dashboard/dashboard', $data);
	}

	// fungsi untuk menampilkan data diri admin yang sedang login
	public function profile()
	{	
		$data['title'] = 'Sistem Lapor Komdisma';

		// $id= user_id();
		// $builder = $this->db->table('users');

		// 	$builder->select('username, email, foto, nama, nim, prodi');
		// 	$builder->join('admin', 'admin.id_akun= users.id');
		// 	$builder->where('users.id', $id);
		// 	$query = $builder->get();

		// 	$data['user'] = $query->getRow();


		return view('mahasiswa/profile', $data);
	}

	// fungsi untuk menampilkan semua data akun yang ada di database
	public function akun()
	{	
		$data['title'] = 'Sistem Lapor Komdisma';

		$builder = $this->db->table('users');
		$builder->select('users.id as userId, username, email, name');
		$builder->join('auth_groups_users', 'auth_groups_users.user_id = users.id');
		$builder->join('auth_groups', 'auth_groups.id = auth_groups_users.group_id');
		$builder->orderBy('userId', 'DESC');
		$query = $builder->get();

		$data['users'] = $query->getResult();

		return view('admin/data/akun', $data);
	}

	// fungsi untuk menampilkan detail dari setiap data akun
	public function detail_akun($id = 0)
	{	
		$data['title'] = 'Sistem Lapor Komdisma';

		
		$builder = $this->db->table('users');

		$builder->select('users.id as userId, username, email, name, foto');
		$builder->join('auth_groups_users', 'auth_groups_users.user_id = users.id');
		$builder->join('auth_groups', 'auth_groups.id = auth_groups_users.group_id');
		$builder->where('users.id', $id);
		$query = $builder->get();

		$data['user'] = $query->getRow();

		if (empty($data['user'])) {
			return redirect()->to('/admin');
		}

		return view('admin/data/detail', $data);
	}

	// fungsi untuk menampilkan data mahasiswa
	public function mahasiswa()
	{	
		$data['title'] = 'Sistem Lapor Komdisma';

		$builder = $this->db->table('users');
		$builder->select('nama, nim, prodi');
		$builder->join('auth_groups_users', 'auth_groups_users.user_id = users.id');
		$builder->join('auth_groups', 'auth_groups.id = auth_groups_users.group_id');
		$builder->join('mahasiswa', 'mahasiswa.id_akun = users.id');
		$builder->where('name', 'Mahasiswa');
		$builder->orderBy('id_akun', 'DESC');
		$query = $builder->get();
		$data['mahasiswas'] = $query->getResult();

		return view('admin/data/mahasiswa', $data);
	}

	// fungsi untuk menambahkan data mahasiswa
	public function addMahasiswa()
	{	
		//hash password
		$nim = strval($this->request->getPost('nim'));
		$password = password_hash(
			base64_encode(
				hash('sha384', $nim, true)
			),
			$this->config->hashAlgorithm,
			$this->hashOptions
		);;

		//get data dari form
		$data_akun = [
			'username' => $this->request->getPost('nim'),
			'password_hash' => $password,
			'email' => $this->request->getPost('nim') . '@gmail.com',
			'active' => 1,
		];
		//query insert data ke tabel user
		$query=$this->userModel->withGroup('Mahasiswa')->insert($data_akun);
		$last_id= $this->userModel->insert_id();
		$id= $last_id->id;

		//menjalankan query insert data ke tabel mahasiswa setelah query insert tabel user berhasil
		if($query){
			$data_mahasiswa= [
				'nama' => $this->request->getPost('nama'),
				'id_akun' => $id,
				'nim' => $this->request->getPost('nim'),
				'prodi' => $this->request->getPost('prodi'),
			];
			$builder = $this->db->table('mahasiswa');
			$builder->insert($data_mahasiswa);
		}
		
		return redirect()->to(base_url('admin/data/mahasiswa'));

	}
}
