<?php namespace App\Models;

use CodeIgniter\Model;

class MahasiswaModel extends Model{
    protected $table      = 'mahasiswa';
    protected $primaryKey = 'nim';

    protected $returnType     = 'object';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['nim', 'nama', 'prodi'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}