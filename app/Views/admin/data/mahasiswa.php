<?= $this->extend('templates/index');?>

<?= $this->section('page-content');?>
<div class="container-fluid">
    <!-- Breadcrumb -->
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?= base_url();?>">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="#">Data Master</a></li>
        <li class="breadcrumb-item active" aria-current="page">Mahasiswa</li>
      </ol>
    </nav>
    <!-- Tambah Mahasiswa -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Tambah Mahasiswa</h6>
            
        </div>
        <div class="card-body">
            <div class="row justify-content-between align-items-center mb-3" >
                <div class="col-6">
                    <a class="btn btn-primary btn-sm mr-2">Import Data Excel</a>
                </div>
                <div class="col-6 text-right">
                    <a class="btn btn-success btn-sm mr-1" data-toggle="tooltip" data-placement="top" title="Tambah Input"><i class="fa fa-plus"></i></a>
                    <a class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus Input"><i class="fa fa-trash"></i></a>
                </div>
            </div>
            <form method="POST" action="<?php echo base_url('admin/addMahasiswa'); ?>">
                <?= csrf_field();?>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Nama</label>
                        <input type="text" class="form-control" name="nama">
                    </div>
                    <div class="form-group col-md-4">
                        <label>NIM</label>
                        <input type="text" class="form-control" name="nim">
                    </div>
                    <div class="form-group col-md-2">
                        <label>Program Studi</label>
                        <select name="prodi" class="form-control">
                        <option selected disabled>Pilih Prodi</option>
                            <option value="A-KMN">A-KMN</option>
                            <option value="B-EKW">B-EKW</option>
                            <option value="C-INF">C-INF</option>
                            <option value="D-TEK">D-TEK</option>
                        </select>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <button type="submit" class="btn btn-primary">Simpan Data</button>
                </div>
            </form>
        </div>
    </div>
    <!-- Table Akun -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Data Mahasiswa</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th scope="row">No</th>
                            <th>NIM</th>
                            <th>Nama</th>
                            <th>Prodi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no=1; 
                            foreach ($mahasiswas as $mahasiswa) : ?>
                        <tr>
                            <td><?= $no++; ?></td>
                            <td><?= $mahasiswa->nim; ?></td>
                            <td><?= $mahasiswa->nama; ?></td>
                            <td><?= $mahasiswa->prodi; ?></td>
                        </tr>
                        <?php endforeach;?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection();?>