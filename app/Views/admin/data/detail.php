<?= $this->extend('templates/index');?>

<?= $this->section('page-content');?>
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">List User</h1>
    <!-- Detail Content -->
    <div class="card shadow mb-4">
        <div class="row no-gutters">
            <div class="col-md-4">
                <img src="<?= base_url('/img/'. $user->foto);?>" class="card-img" alt="<?= $user->username;?>">
            </div>
                <div class="col-md-8  d-flex align-items-center">
                <div class="card-body">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item"><?= $user->username;?></li>
                        <li class="list-group-item"><?= $user->email;?></li>
                        <li class="list-group-item">
                            <span class="badge badge-outline-primary"><?= $user->name;?></span>
                        </li>
                        <li class="list-group-item">
                            <a href="<?= base_url('admin/data/akun');?>" class="small">&laquo; kembali ke user list</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection();?>