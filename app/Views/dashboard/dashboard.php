<?= $this->extend('templates/index');?>

<?= $this->section('page-content');?>
<div class="container-fluid">

<?php if (in_groups('Admin')) : ?>
    <!-- Dashboard Admin -->
        <?= $this->include('admin/dashboard'); ?>
    <!-- End of dashboard-->

<?php elseif (in_groups('Akademik')) : ?>
    <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800">Dashboard Akademik</h1>
    </div>

<?php elseif (in_groups('Dosen')) : ?>
    <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800">Dashboard Dosen</h1>
    </div>

<?php elseif (in_groups('Mahasiswa')) : ?>
    <!-- Dashboard Mahasiswa -->
        <?= $this->include('mahasiswa/dashboard'); ?>
    <!-- End of dashboard-->

<?php endif;?>
</div>
<?= $this->endSection();?>