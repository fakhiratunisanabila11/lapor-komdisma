<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div class="sidebar-brand-icon">
                    <i class='bx bxs-conversation'></i>
                </div>
                <div class="sidebar-brand-text mx-3">SILAKOM</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="index.html">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>

            <?php if (in_groups('Admin')) : ?>

                <li class="nav-item">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseData"
                        aria-expanded="true" aria-controls="collapseData">
                        <i class="fas fa-folder"></i>
                        <span>Data Master</span>
                    </a>
                    <div id="collapseData" class="collapse" aria-labelledby="headingData"
                        data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">
                            <!-- <h6 class="collapse-header">Data Master:</h6> -->
                            <a class="collapse-item" href="<?= base_url('admin/data/akun');?>">Akun</a>
                            <a class="collapse-item" href="<?= base_url('admin/data/admin');?>">Admin</a>
                            <a class="collapse-item" href="utilities-animation.html">Akademik</a>
                            <a class="collapse-item" href="utilities-other.html">Dosen</a>
                            <a class="collapse-item" href="<?= base_url('admin/data/mahasiswa');?>">Mahasiswa</a>
                        </div>
                    </div>
                </li>
            <?php endif;?>
            

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Addons
            </div>
            <!-- Nav Item - Profile -->
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url('user');?>">
                    <i class="fas fa-user"></i>
                    <span>My Profile</span></a>
            </li>

            <!-- Nav Item - Edit Profile -->
            <li class="nav-item">
                <a class="nav-link" href="tables.html">
                    <i class="fas fa-user-edit"></i>
                    <span>Edit Profile</span></a>
            </li>

            <!-- Nav Item - Logout -->
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url('logout');?>">
                    <i class="fas fa-sign-out-alt"></i>
                    <span>Logout</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">


             <!-- Sidebar Toggler (Sidebar) -->
             <!-- <div class="text-center d-none d-md-inline">
                        <button class="rounded-circle border-0" id="sidebarToggle"></button>
                    </div> -->

           

        </ul>