<?= $this->extend('auth/templates/index');?> 
 
<?= $this->section('content'); ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5 col-sm-12">
            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                            <div class="p-5">
                                <div class="text-center">
                                        <a class="navbar-brand mb-2" href="#">
                                            <img src="<?= base_url(); ?>/img/logo.png" width="45%" alt="">
                                        </a>
                                </div>

                                <?= view('Myth\Auth\Views\_message_block') ?>

                                <form action="<?= route_to('register') ?>" method="post" class="user mt-3">
                                    <?= csrf_field() ?>
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-user <?php if(session('errors.username')) : ?>is-invalid<?php endif ?>" id="username" name="username" placeholder="<?=lang('Auth.username')?>" value="<?= old('username') ?>">
                                    </div>
                                    <div class="form-group">
                                        <input type="email" class="form-control form-control-user <?php if(session('errors.email')) : ?>is-invalid<?php endif ?>" name="email" id="email" placeholder="<?=lang('Auth.email')?>" value="<?= old('email') ?>">
                                    </div>
                                    <div class="form-group row pb-3">
                                        <div class="col-sm-6 mb-3 mb-sm-0">
                                            <input type="password" class="form-control form-control-user <?php if(session('errors.password')) : ?>is-invalid<?php endif ?>" id="password" name="password" placeholder="<?=lang('Auth.password')?>" autocomplete="off">
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="password" class="form-control form-control-user <?php if(session('errors.pass_confirm')) : ?>is-invalid<?php endif ?>" name="pass_confirm" id="pass_confirm" placeholder="Ulang Password" autocomplete="off">
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-user btn-block">
                                    <?=lang('Auth.register')?>
                                    </button>
                                </form>
                                <hr>
                                <div class="text-center">
                                    <p class="small">Sudah punya akun? <a href="<?= route_to('login') ?>">Login</a></p>
                                </div>
                            </div>
                    </div>

                    

                </div>
            </div>
        </div>
        
    </div>
    <div class="row text-white justify-content-center small">
            Copyright &copy; Komisi Disiplin dan Kemahasiswaan Sekolah Vokasi IPB <?= date('Y'); ?>. All rights reserved.
    </div>
</div>
<?= $this->endSection(); ?>
